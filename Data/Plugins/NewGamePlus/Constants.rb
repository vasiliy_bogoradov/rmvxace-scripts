module DF
  module NewGamePlus
    module Constants
      NG_SWITCH_NUMBER = 100

      NG_WINDOW_TONE = Tone.new(-255, -255, -255)
      NG_TEXT_COMMAND = "NG+"
      NG_TEXT_SAVE_TEXT = "NG+ File"
      NOT_NG_TEXT_SAVE_TEXT = "Casual save"
      NG_HELP_WINDOW_TEXT = "Start New Game+"
    end
  end
end