class Window_SaveFile < Window_Base
  def initialize(height, index)
    super(0, index * height, Graphics.width, height)
    @file_index = index
    refresh
    @selected = false

  end

  def new_game_plus_save_text
    DF::NewGamePlus::Constants::NG_TEXT_SAVE_TEXT + " #{@file_index + 1}"
  end

  def draw_new_game_plus_context(opts={})
    opts = {
      enabled: true,
      name: new_game_plus_save_text,
      text_color: crisis_color,
    }.merge(opts)

    draw_save_context(opts)
  end

  def draw_save_context(opts={})
    opts = {
      enabled: true,
      name: Vocab::File + " #{@file_index + 1}",
      text_color: normal_color
    }.merge(opts)
    change_color(opts[:text_color], opts[:enabled])
    draw_text(4, 0, 200, line_height, opts[:name])
    @name_width = text_size(opts[:name]).width
    draw_party_characters(152, 58)
    draw_playtime(0, contents.height - line_height, contents.width - 4, 2)
  end

  def new_game_plus_save?
    @header = DataManager.load_header(@file_index)
    DF::NewGamePlus::Selectors::it_new_game_plus_save?(@header)
  end


  def refresh
    contents.clear

    if new_game_plus_save?
      draw_new_game_plus_context
      return nil
    end

    draw_save_context
  end

end