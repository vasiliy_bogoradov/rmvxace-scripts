class Scene_Map < Scene_Base

  alias old_update update

  def update
    super
    if DF::SpeedUp::Selectors::is_fast_forward?
      (0...DF::SpeedUp::Settings::SPEED_UP_TIMES).each { |_|
        one_time_update
      }
    end
    one_time_update

  end

  def one_time_update
    $game_map.update(true)
    $game_player.update
    $game_timer.update
    @spriteset.update
    update_scene if scene_change_ok?
  end

end

class Window_Message < Window_Base
  def input_pause
    if DF::SpeedUp::Selectors::is_fast_forward?
      @show_fast = true
      self.pause = false
      return nil
    end

    self.pause = true
    wait(10)
    Fiber.yield until Input.trigger?(:B) || Input.trigger?(:C)
    Input.update
    self.pause = false
  end
  def update
    super
    if DF::SpeedUp::Selectors::is_fast_forward?
      one_time_update
    end
    one_time_update

  end

  def one_time_update
    update_all_windows
    update_back_sprite
    update_fiber
  end
end


module DF
  module SpeedUp
    module Settings
      SPEED_UP_TIMES = 10
    end
    module Selectors
      def self.is_fast_forward?
        return Input.press?(:CTRL)
      end
    end
  end
end
