$imported = {} if $imported.nil?
$imported["NewGamePlus"] = true

puts "New Game Plus Script Loaded"
module DF
  module NewGamePlus
    module Constants
      NG_SWITCH_NUMBER = 100

      NG_WINDOW_TONE = Tone.new(-255, -255, -255)
      NG_TEXT_COMMAND = "NG+"
      NG_TEXT_SAVE_TEXT = "NG+ File"
      NOT_NG_TEXT_SAVE_TEXT = "Casual save"
      NG_HELP_WINDOW_TEXT = "Start New Game+"
    end
  end
end
module DF
  module NewGamePlus
    module Selectors

      def self.it_new_game_plus_save?(headers)
        if save_file_exists?(headers)
          return headers[:switches][DF::NewGamePlus::Constants::NG_SWITCH_NUMBER]
        end
        return false
      end

      def self.save_file_exists?(headers)
        return false if headers.nil?
        return false if headers[:switches].nil?
        return true
      end

    end
  end
end

module DataManager

  def self.setup_new_game_plus(index)
    create_new_game_plus_objects(index)
    $game_map.setup($data_system.start_map_id)
    $game_player.moveto($data_system.start_x, $data_system.start_y)
    $game_player.refresh
    Graphics.frame_count = 0
  end

  def self.create_new_game_plus_objects(index)
    load_game_without_rescue(index)
    new_game_plus_reset_switches
    new_game_plus_reset_variables
    new_game_plus_reset_self_switches
    new_game_plus_reset_actors
    new_game_plus_reset_party
  end

  def self.new_game_plus_reset_switches
    $game_switches = Game_Switches.new
    $game_switches[DF::NewGamePlus::Constants::NG_SWITCH_NUMBER] = true
  end

  def self.new_game_plus_reset_self_switches
    $game_self_switches = Game_SelfSwitches.new
  end

  def self.new_game_plus_reset_variables
    (0...$data_system.variables.size).each { |i|
      $game_variables[i] = 0
    }
  end

  def self.new_game_plus_reset_actors
    $game_actors = Game_Actors.new
  end

  def self.new_game_plus_reset_party
    $game_party = Game_Party.new
    $game_party.setup_starting_members
  end

  def self.make_save_header
    header = {}
    header[:characters]    = $game_party.characters_for_savefile
    header[:playtime_s]    = $game_system.playtime_s
    header[:system]        = Marshal.load(Marshal.dump($game_system))
    header[:timer]         = Marshal.load(Marshal.dump($game_timer))
    header[:message]       = Marshal.load(Marshal.dump($game_message))
    header[:switches]      = Marshal.load(Marshal.dump($game_switches))
    header[:variables]     = Marshal.load(Marshal.dump($game_variables))
    header[:self_switches] = Marshal.load(Marshal.dump($game_self_switches))
    header[:actors]        = Marshal.load(Marshal.dump($game_actors))
    header[:party]         = Marshal.load(Marshal.dump($game_party))
    header[:troop]         = Marshal.load(Marshal.dump($game_troop))
    header[:map]           = Marshal.load(Marshal.dump($game_map))
    header[:player]        = Marshal.load(Marshal.dump($game_player))
    header
  end

  def self.new_game_plus_saves_exists?
    Dir.glob('Save*.rvdata2').any? { |filename|
      headers = load_header_without_rescue_by_name(filename)
      DF::NewGamePlus::Selectors::it_new_game_plus_save?(headers)
    }
  end


  def self.load_header_without_rescue_by_name(filename)
    File.open(filename, 'rb') do |file|
      return Marshal.load(file)
    end
    nil
  end
end

class Scene_Load < Scene_File

  def on_savefile_ok
    super

    if !@savefile_windows[@index].new_game_plus_save? and DataManager.load_game(@index)
      on_load_success
      return nil
    end

    if @savefile_windows[@index].new_game_plus_save? and DataManager.setup_new_game_plus(@index)
      on_load_success
      return nil
    end

    Sound.play_buzzer
  end
end
class Window_SaveFile < Window_Base
  def initialize(height, index)
    super(0, index * height, Graphics.width, height)
    @file_index = index
    refresh
    @selected = false

  end

  def new_game_plus_save_text
    DF::NewGamePlus::Constants::NG_TEXT_SAVE_TEXT + " #{@file_index + 1}"
  end

  def draw_new_game_plus_context(opts={})
    opts = {
      enabled: true,
      name: new_game_plus_save_text,
      text_color: crisis_color,
    }.merge(opts)

    draw_save_context(opts)
  end

  def draw_save_context(opts={})
    opts = {
      enabled: true,
      name: Vocab::File + " #{@file_index + 1}",
      text_color: normal_color
    }.merge(opts)
    change_color(opts[:text_color], opts[:enabled])
    draw_text(4, 0, 200, line_height, opts[:name])
    @name_width = text_size(opts[:name]).width
    draw_party_characters(152, 58)
    draw_playtime(0, contents.height - line_height, contents.width - 4, 2)
  end

  def new_game_plus_save?
    @header = DataManager.load_header(@file_index)
    DF::NewGamePlus::Selectors::it_new_game_plus_save?(@header)
  end


  def refresh
    contents.clear

    if new_game_plus_save?
      draw_new_game_plus_context
      return nil
    end

    draw_save_context
  end

end
class Window_SaveFile_NewGamePlus < Window_SaveFile
    def initialize(height, index)
      super(height, index)

    end
  def new_game_plus_save_text
    DF::NewGamePlus::Constants::NG_TEXT_SAVE_TEXT
  end

  def refresh
    contents.clear
    if new_game_plus_save?
      draw_new_game_plus_context({ enabled: true })
      return nil
    end
    draw_save_context({ enabled: false })
  end

    def update_tone
      puts "update tone"
      self.tone.set(DF::NewGamePlus::Constants::NG_WINDOW_TONE)
    end

end
# Fullscreen++ v2.2 for VX and VXace by Zeus81
# Free for non commercial and commercial use
# Licence : http://creativecommons.org/licenses/by-sa/3.0/
# Contact : zeusex81@gmail.com
# (fr) Manuel d'utilisation : http://pastebin.com/raw.php?i=1TQfMnVJ
# (en) User Guide           : http://pastebin.com/raw.php?i=EgnWt9ur

$imported ||= {}
$imported[:Zeus_Fullscreen] = __FILE__

class << Graphics
  Disable_VX_Fullscreen = false

  CreateWindowEx = Win32API.new('user32', 'CreateWindowEx', 'ippiiiiiiiii', 'i')
  GetClientRect = Win32API.new('user32', 'GetClientRect', 'ip', 'i')
  GetDC = Win32API.new('user32', 'GetDC', 'i', 'i')
  GetSystemMetrics = Win32API.new('user32', 'GetSystemMetrics', 'i', 'i')
  GetWindowRect = Win32API.new('user32', 'GetWindowRect', 'ip', 'i')
  FillRect = Win32API.new('user32', 'FillRect', 'ipi', 'i')
  FindWindow = Win32API.new('user32', 'FindWindow', 'pp', 'i')
  ReleaseDC = Win32API.new('user32', 'ReleaseDC', 'ii', 'i')
  SendInput = Win32API.new('user32', 'SendInput', 'ipi', 'i')
  SetWindowLong = Win32API.new('user32', 'SetWindowLong', 'iii', 'i')
  SetWindowPos = Win32API.new('user32', 'SetWindowPos', 'iiiiiii', 'i')
  ShowWindow = Win32API.new('user32', 'ShowWindow', 'ii', 'i')
  SystemParametersInfo = Win32API.new('user32', 'SystemParametersInfo', 'iipi', 'i')
  UpdateWindow = Win32API.new('user32', 'UpdateWindow', 'i', 'i')
  GetPrivateProfileString = Win32API.new('kernel32', 'GetPrivateProfileString', 'ppppip', 'i')
  WritePrivateProfileString = Win32API.new('kernel32', 'WritePrivateProfileString', 'pppp', 'i')
  CreateSolidBrush = Win32API.new('gdi32', 'CreateSolidBrush', 'i', 'i')
  DeleteObject = Win32API.new('gdi32', 'DeleteObject', 'i', 'i')

  unless method_defined?(:zeus_fullscreen_update)
    HWND = FindWindow.call('RGSS Player', 0)
    BackHWND = CreateWindowEx.call(0x08000008, 'Static', '', 0x80000000, 0, 0, 0, 0, 0, 0, 0, 0)
    alias zeus_fullscreen_resize_screen resize_screen
    alias zeus_fullscreen_update update
  end

  private

  def initialize_fullscreen_rects
    @borders_size ||= borders_size
    @fullscreen_rect ||= screen_rect
    @workarea_rect ||= workarea_rect
  end

  def borders_size
    GetWindowRect.call(HWND, wrect = [0, 0, 0, 0].pack('l4'))
    GetClientRect.call(HWND, crect = [0, 0, 0, 0].pack('l4'))
    wrect, crect = wrect.unpack('l4'), crect.unpack('l4')
    Rect.new(0, 0, wrect[2] - wrect[0] - crect[2], wrect[3] - wrect[1] - crect[3])
  end

  def screen_rect
    Rect.new(0, 0, GetSystemMetrics.call(0), GetSystemMetrics.call(1))
  end

  def workarea_rect
    SystemParametersInfo.call(0x30, 0, rect = [0, 0, 0, 0].pack('l4'), 0)
    rect = rect.unpack('l4')
    Rect.new(rect[0], rect[1], rect[2] - rect[0], rect[3] - rect[1])
  end

  def hide_borders()
    SetWindowLong.call(HWND, -16, 0x14000000)
  end

  def show_borders()
    SetWindowLong.call(HWND, -16, 0x14CA0000)
  end

  def hide_back()
    ShowWindow.call(BackHWND, 0)
  end

  def show_back
    ShowWindow.call(BackHWND, 3)
    UpdateWindow.call(BackHWND)
    dc = GetDC.call(BackHWND)
    rect = [0, 0, @fullscreen_rect.width, @fullscreen_rect.height].pack('l4')
    brush = CreateSolidBrush.call(0)
    FillRect.call(dc, rect, brush)
    ReleaseDC.call(BackHWND, dc)
    DeleteObject.call(brush)
  end

  def resize_window(w, h)
    if @fullscreen
      x, y, z = (@fullscreen_rect.width - w) / 2, (@fullscreen_rect.height - h) / 2, -1
    else
      w += @borders_size.width
      h += @borders_size.height
      x = @workarea_rect.x + (@workarea_rect.width - w) / 2
      y = @workarea_rect.y + (@workarea_rect.height - h) / 2
      z = -2
    end
    SetWindowPos.call(HWND, z, x, y, w, h, 0)
  end

  def release_alt
    inputs = [1, 18, 2, 1, 164, 2, 1, 165, 2].pack('LSx2Lx16' * 3)
    SendInput.call(3, inputs, 28)
  end

  public

  def load_fullscreen_settings
    buffer = [].pack('x256')
    section = 'Fullscreen++'
    filename = './Game.ini'
    get_option = Proc.new do |key, default_value|
      l = GetPrivateProfileString.call(section, key, default_value, buffer, buffer.size, filename)
      buffer[0, l]
    end
    @fullscreen = get_option.call('Fullscreen', '0') == '1'
    @fullscreen_ratio = get_option.call('FullscreenRatio', '0').to_i
    @windowed_ratio = get_option.call('WindowedRatio', '1').to_i
    toggle_vx_fullscreen if Disable_VX_Fullscreen and vx_fullscreen?
    fullscreen? ? fullscreen_mode : windowed_mode
  end

  def save_fullscreen_settings
    section = 'Fullscreen++'
    filename = './Game.ini'
    set_option = Proc.new do |key, value|
      WritePrivateProfileString.call(section, key, value.to_s, filename)
    end
    set_option.call('Fullscreen', @fullscreen ? '1' : '0')
    set_option.call('FullscreenRatio', @fullscreen_ratio)
    set_option.call('WindowedRatio', @windowed_ratio)
  end

  def fullscreen?
    @fullscreen or vx_fullscreen?
  end

  def vx_fullscreen?
    rect = screen_rect
    rect.width == 640 and rect.height == 480
  end

  def toggle_fullscreen
    fullscreen? ? windowed_mode : fullscreen_mode
  end

  def toggle_vx_fullscreen
    windowed_mode if @fullscreen and !vx_fullscreen?
    inputs = [1, 18, 0, 1, 13, 0, 1, 13, 2, 1, 18, 2].pack('LSx2Lx16' * 4)
    SendInput.call(4, inputs, 28)
    zeus_fullscreen_update
    self.ratio += 0 # refresh window size
  end

  def vx_fullscreen_mode
    return if vx_fullscreen?
    toggle_vx_fullscreen
  end

  def fullscreen_mode
    return if vx_fullscreen?
    initialize_fullscreen_rects
    show_back
    hide_borders
    @fullscreen = true
    self.ratio += 0 # refresh window size
  end

  def windowed_mode
    toggle_vx_fullscreen if vx_fullscreen?
    initialize_fullscreen_rects
    hide_back
    show_borders
    @fullscreen = false
    self.ratio += 0 # refresh window size
  end

  def toggle_ratio
    return if vx_fullscreen?
    self.ratio += 1
  end

  def ratio
    return 1 if vx_fullscreen?
    @fullscreen ? @fullscreen_ratio : @windowed_ratio
  end

  def ratio=(r)
    return if vx_fullscreen?
    initialize_fullscreen_rects
    r = 0 if r < 0
    if @fullscreen
      @fullscreen_ratio = r
      w_max, h_max = @fullscreen_rect.width, @fullscreen_rect.height
    else
      @windowed_ratio = r
      w_max = @workarea_rect.width - @borders_size.width
      h_max = @workarea_rect.height - @borders_size.height
    end
    if r == 0
      w, h = w_max, w_max * height / width
      h, w = h_max, h_max * width / height if h > h_max
    else
      w, h = width * r, height * r
      return self.ratio = 0 if w > w_max or h > h_max
    end
    resize_window(w, h)
    save_fullscreen_settings
  end

  def update
    release_alt if Disable_VX_Fullscreen and Input.trigger?(Input::ALT)
    zeus_fullscreen_update
    toggle_fullscreen if Input.trigger?(Input::F5)
    toggle_ratio if Input.trigger?(Input::F6)
  end

  def resize_screen(width, height)
    zeus_fullscreen_resize_screen(width, height)
    self.ratio += 0 # refresh window size
  end
end

Graphics.load_fullscreen_settings
class Scene_Map < Scene_Base

  alias old_update update

  def update
    super
    if DF::SpeedUp::Selectors::is_fast_forward?
      (0...DF::SpeedUp::Settings::SPEED_UP_TIMES).each { |_|
        one_time_update
      }
    end
    one_time_update

  end

  def one_time_update
    $game_map.update(true)
    $game_player.update
    $game_timer.update
    @spriteset.update
    update_scene if scene_change_ok?
  end

end

class Window_Message < Window_Base
  def input_pause
    if DF::SpeedUp::Selectors::is_fast_forward?
      @show_fast = true
      self.pause = false
      return nil
    end

    self.pause = true
    wait(10)
    Fiber.yield until Input.trigger?(:B) || Input.trigger?(:C)
    Input.update
    self.pause = false
  end
  def update
    super
    if DF::SpeedUp::Selectors::is_fast_forward?
      one_time_update
    end
    one_time_update

  end

  def one_time_update
    update_all_windows
    update_back_sprite
    update_fiber
  end
end


module DF
  module SpeedUp
    module Settings
      SPEED_UP_TIMES = 10
    end
    module Selectors
      def self.is_fast_forward?
        return Input.press?(:CTRL)
      end
    end
  end
end

module Input
  F10 = :F10
end

